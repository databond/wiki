# Chapter 3: Plugin Creation
Plugins represent the most important feature of Databond. It is what allows the user to interact with more than one type of Database. 
However, this application was only created by graduate students who have no time or enough knowledge to cover all the RDBMSes out there so here we will tell you how you can do it so you can use it with whatever RDBMS you want.

## Pre-requisites 
C++/Qt knowledge is required.

## What is a plugin?
A plugin is basically a dll that has the necessary code to interact with the RDBMS of choice.

## How to make your own plugin
Since Databond was created using the Qt framework, we have used constructs that facilitate the creation of plugins provided by the framework. 
Two abstract classes matter to us in this situation: 
- The `QSqlDriver` abstract class
- The `QSqlResult` abstract class

a thorough explanation of both of these can be found in the [official website](https://doc.qt.io/qt-6.2/sql-driver.html#development).

To make things easier, Databond has the `Plugin` abstract class which subclasses `QSqlDriver` and adds a couple of functions that we will describe below, so to implement a plugin you'd have to implement the Plugin class, make a .dll out of it or an .so if you're on Linux, load it at runtime and you're good to go.

***Note:** That if you build Databond from source with a different compiler than the one you used to build the plugin, it is not going to work, make sure you have used the same compiler*


## The `Plugin` abstract class
We will describe the added functions because the rest are documented at the official website.

### columns(QSql::TableType type, const QString& tableName) const -> QStringList
- **Parameters** : 
    + `type` : The type of the table this function will operate on, such as a view, regular table, system tables..etc
    + `tableName` : The name of the table.
- **Return Value** : A `QStringList` value
- **Functionality** : Returns a list of QString values representing the column names of the specified table type.

### columnsWithType(QSql::TableType type, const QString& tableName) const -> QList<QPair<QString, QString>>
- **Parameters** : 
    + `type` : The type of the table this function will operate on, such as a view regular table, system tables..etc.
    + `tableName` : The name of the table.
- **Return Value** : A `QList` of `QPair`s of `<QString, QString>` type. 
- **Functionality** : Returns a list of tuples in which each one represents the name of the column and its type. 
### types() const -> QStringList
- **Parameters** : None.
- **Return Value** : A `QStringList` value.
- **Functionality** : Returns the types of columns supported by the RDBMS.

### isWithNetwork() const -> bool
- **Parameters** : None.
- **Return Value** : A `boolean` value.
- **Functionality** : Checks whether the connection to the database is over a network or not. 

### triggers() const noexcept -> QStringList
- **Parameters** : None.
- **Return Value** : A `QStringList` value.
- **Functionality** : Returns a list of strings representing the names of triggers of the 

### availableOptions() const noexcept -> QStringLis
- **Parameters** : None.
- **Return Value** : A `QStringList` value.
- **Functionality** : Returns a list of strings representing the available options by the RDBMS this plugin is implemented for.

### foreignKeys(const QString& table) const noexcept -> QList<QPair<QString, QString>>
- **Parameters** : 
    + `table` : The name of the table this operation will use.
- **Return Value** : A `QList` of `QPair`s of `<QString, QString>` type. 
- **Functionality** : Returns a list of tuples with the first field of the tuple representing the column of the foreign key and the second one representing the table that is referred to.


