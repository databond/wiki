# Chapter 1: An Introduction to Databond!

Databond is a universal database client that facilitates interaction with **relational** DBMSes. It was designed with flexibility and modularity in mind.

It is still quite immature compared to some other tools in the same category but with more effort and maybe some help from the community, it can be further improved!
## Why should you use it instead of X ?

Because it was developed in C++, which is faster than your language :) 
just kidding, there is no reason to use it instead the other feature-ready applications, but with enough time it might become good enough to compete.
## How can I contribute ?

You need some beginner-intermediate level of Qt/C++ knowledge and you're good to go.

## How does it work ?

TODO: add diagrams and an explanation of each component and its modules