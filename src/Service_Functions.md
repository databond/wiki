# Chapter 2: Service Functions

This chapter will list and provide an explanation of the RPC interface functions. Before diving in, we will clarify some things.
## Why MessagePack? 

Because the RPC server that was used to develop this app uses it. ~~Yes we hate it too, but at least it's fast.~~

## Will you support other data formats?

Maybe.

## What's a Session?
A session is simply a group of connections in Databond. It was made with organization purposes in mind and to preserve state because re-opening your connections every single time is tiresome.
## What's a Connection?
A regular connection to a database with some abstraction from our side. 
## Functions list

This section will list each function under the category that it belongs to, and provide an explanation in the following format: 

- The function signature: it does not *exactly* look like that in the source code, this is just an approximation.
- The parameters: elaboration will be provided if necessary.
- What the function does.
- What it returns: elaboration will be provided if necessary. If the function returns nothing, nothing will be written in the return type, not even `void`.
- Possible errors: only if they matter.
- additional notes: if they don't fit in any of the above points.

___

### Special functions
#### request()
The reason why the `request` function was given its own section is because it does not belong to any of the categories below. It has to be called by every client before starting a session or using any other function, otherwise, **an error will be returned**. 

What it does is basically create a new client instance in the server(Databond). ~~Calling this function twice results in returning an error.~~

### Session functions
Session-related functions are listed here.\
***Note**: calling any of these functions before `request` results in returning an error.*

#### session.new(): int 

- **Parameters**: None.
- **Return value**: The ID of the newly created session.
- **Functionality**: Creates a new session in the server and returns its ID to the user for future use.


#### session.setName(id: int, sessionName: string)
- **Parameters**: 
    + `id` : The ID of the session in use.
    + `sessionName` : The name of the session the caller provides.
- **Return value**: None.
- **Functionality**: Sets the name of the session.
#### session.newWithName(sessionName: string): int 
- **Parameters**: 
    + `sessionName` : The name of the session the caller provides.
- **Return value**: None.
- **Functionality**: Creates a new session with the provided name.

#### session.exist(id: int): bool
- **Parameters**: 
    + `id` : ID of the session.
- **Return value**: A boolean value.
- **Functionality**: Checks if the session exists or not based on the provided ID.

#### session.count(): int 
- **Parameters**: None.
- **Return value**: An `int` value
- **Functionality**: Returns the number that represents the number of the sessions of the client.

#### session.close(id: int) 
- **Parameters**:
    + `id` : The ID of the session.
- **Return value**: None.
- **Functionality**: Closes the session and its connections, if any.
#### session.name(id: int): string
- **Parameters**:
    + `id` : The ID of the session.
- **Return value**: A `string` value.
- **Functionality**: Returns the name of the session.

#### session.file.save(id: int, path: string): bool
- **Parameters**:
    + `id` : The ID of the session.
    + `path` : The path of the file.
- **Return value**: A `boolean` value if the file is correctly saved.
- **Functionality**: Saves the session's information in a file in JSON syntax.
- **Possible Errors**: It could fail because for various reasons such as bad formatting, missing .bond suffix, permission related problems..etc
-- **Additional Notes**: The file *MUST* have the `.bond` suffix or an Error 
#### session.file.open(path: string): int 
- **Parameters**:
    + `path` : The path of the file.
- **Return value**: An `int` value if the file is correctly loaded
- **Functionality**: Loads a session saved in a file and returns its ID for future use.
- **Possible Errors**: It could fail because for various reasons such as bad formatting, missing .bond suffix, permission related problems..etc
-- **Additional Notes**: The file *MUST* have the `.bond` suffix.

---

#### Connection functions
Connection-related functions are listed here.\
***Note**: calling any of these functions before `request` results in returning an error.*
#### connection.addConnection(id: int, pluginName: string, conName: string)
- **Parameters**:
    + `id` : The ID of the session.
    + `pluginName` : The name of the plugin used for this new connection.
    + `conName` : The name of the connection.
- **Return value**: None.
- **Functionality**: Adds a new connection in the server without opening it.

#### connection.exist(id: int, conName: string): bool 
- **Parameters**:
    + `id` : The ID of the session.
    + `conName` : The name of the connection.
- **Return value**: A `boolean` value.
- **Functionality**: Checks whether a connection with the provided information exists.

#### connection.pluginName(id: int, conName: string): string 
- **Parameters**:
    + `id` : The ID of the session.
    + `conName` : The name of the connection.
- **Return value**: A `string` value
- **Functionality**: Returns a string representing the name of the plugin in use in this connection.

#### connection.availablePlugins(): string[] 
- **Parameters**: None.
- **Return value**: An array of `string`.
- **Functionality**: Returns the available plugins.

#### connection.open(id: int, conName: string): bool
- **Parameters**:
    + `id` : The ID of the session.
    + `conName` : The name of the connection.
- **Return value**: A `boolean` type and specifically `true`e if the connection is opened correctly.
- **Functionality**: Opens a connection through its name.
- **Possible Errors**: An error could occur if the session does not exist or if the connection does not exist.

#### connection.count(id: int): int 
- **Parameters**:
    + `id` : the ID of the session.
- **Return value**: An `int` value.
- **Functionality**: Returns the count of the connections in a specific session.

#### connection.commit(id: int, conName: string): bool
- **Parameters**:
    + `id` : The ID of the session.
    + `conName` : The name of the connection.
- **Return value**: A `boolean` value.
- **Functionality**: Reflects changes in the database after all the statements in the transaction are done.

#### connection.transaction(id: int, conName: string): bool
- **Parameters**:
    + `id` : The ID of the session.
    + `conName` : The name of the connection.
- **Return value**: A `boolean` value.
- **Functionality**: Starts a transaction and returns `true` if the transaction starts correctly.

#### connection.rollback(id: int, conName: string): bool
- **Parameters**:
    + `id` : The ID of the session.
    + `conName` : The name of the connection.
- **Return value**: A `boolean` value.
- **Functionality**: Rollsback the transaction and returns `true` if it is rolled back correctly.

#### connection.isOpen(id: int, conName: string): bool
- **Parameters**:
    + `id` : The ID of the session.
    + `conName` : The name of the connection.
- **Return value**: A `boolean` value.
- **Functionality**: Checks whether a conneciton is open or not, returning `true` if it is open.

#### connection.close(id: int, conName: string)
- **Parameters**:
    + `id` : The ID of the session.
    + `conName` : The name of the connection.
- **Return value**: None.
- **Functionality**: Closes the specified connection

#### connection.options(id: int, conName: string): string
- **Parameters**:
    + `id` : The ID of the session.
    + `conName` : The name of the connection.
- **Return value**: A `string` value.
- **Functionality**: Returns a semi-colon separated string of key-value expressions representing the options of the specified connection.

#### connection.databaseName(id: int, conName: string): string
- **Parameters**:
    + `id` : The ID of the session.
    + `conName` : The name of the connection.
- **Return value**: A `string` value.
- **Functionality**: Returns the database name of the specified connection.

#### connection.execute(id: int, conName: string, query: string): string 
- **Parameters**:
    + `id` : The ID of the session.
    + `conName` : The name of the connection.
    + `query` : A String representing the query to be executed.
- **Return value**: A `string` value that is JSON-formatted.\
for `SELECT` statements, it is going to look like this: 
```
 {
  "Query": "query"
  "Result": {
      "Column1": [
      ],
      "Column2": [
      ]
  }
 }
```

As for other statements, it's going to be like this: 
```
{
 "Query" : "query",
 "Result" : true|false
}
```
- **Functionality**: Executes the query string passed by the caller.

#### connection.username(id: int, conName: string): string
- **Parameters**:
    + `id` : The ID of the session.
    + `conName` : The name of the connection.
- **Return value**: A `string` value.
- **Functionality**: Returns the username of the account that is currently using the database.

#### connection.hostname(id: int, conName: string): string
- **Parameters**:
    + `id` : The ID of the session.
    + `conName` : The name of the connection.
- **Return value**: A `string` value.
- **Functionality**: Returns the hostname which can be a normal username-like string or an IP address of the database.

#### connection.lastSqlError(id: int, conName: string): SqlError
- **Parameters**:
    + `id` : The ID of the session.
    + `conName` : The name of the connection.
- **Return value**: A `SqlError` instance that looks like the following:\
```
SqlError{
   string errorCode; // Native database error code
   string type; // Connection|Statement|Transaction|Unkown|No Error
   string text; // Error description
}
```
- **Functionality**: Returns the last error that occured when executing a query.

#### connection.password(id: int, conName: string): string
- **Parameters**:
    + `id` : The ID of the session.
    + `conName` : The name of the connection.
- **Return value**: A `string` value.
- **Functionality**: Returns the password of the account that is currently used to access the database.

#### connection.port(id: int, conName: string): int
- **Parameters**:
    + `id` : The ID of the session.
    + `conName` : The name of the connection.
- **Return value**: An `int` value.
- **Functionality**: Returns the port that the database server is running on

#### connection.setOptions(id: int, conName: string, options: string)
- **Parameters**:
    + `id` : The ID of the session.
    + `conName` : The name of the connection.
    + `options` : A semi-colon separated key-value expressions representing the options to be set by the caller.
- **Return value**: None.
- **Functionality**: Sets the options of the connection. For this to take effect the connection must be re-opened.

#### connection.setDatabaseName(id: int, conName: string, databaseName: string)
- **Parameters**:
    + `id` : The ID of the session.
    + `conName` : The name of the connection.
    + `databaseName` : The name of the Database to be set by the caller.
- **Return value**: None.
- **Functionality**: Sets the database name.

#### connection.setHostname(id: int, conName: string, hostname: string)
- **Parameters**:
    + `id` : The ID of the session.
    + `conName` : The name of the connection.
    + `hostname` : The hostname of the server running the database.
- **Return value**: None.
- **Functionality**: Sets the hostname of the server running the database.

#### connection.setPort(id: int, conName: string, port: int) 
- **Parameters**:
    + `id` : The ID of the session.
    + `conName` : The name of the connection.
    + `port` : the port where the database service is running.
- **Return value**: None.
- **Functionality**: Sets the port of the database service.

#### connection.setUsername(id: int, conName: string, username: string) 
- **Parameters**:
    + `id` : The ID of the session.
    + `conName` : The name of the connection.
    + `username` : The username of the account using the database.
- **Return value**: None.
- **Functionality**: Sets the username of the account accessing the database.

#### connection.tables(id: int, conName: string, type: string): string[]
- **Parameters**:
    + `id` : The ID of the session.
    + `conName` : The name of the connection.
    + `type` : The type of the result to be queries for, either `views` or `tables`.
- **Return value**: An array of `string` values.
- **Functionality**: Returns the views or the tables in the database depending on what the caller specifies.

#### connection.columns(id: int, conName: string, tableName: string): string[] 
- **Parameters**:
    + `id` : The ID of the session.
    + `conName` : The name of the connection.
    + `tableName` : The name of the table that this operation will use.
- **Return value**: An array of `string` values.
- **Functionality**: Returns the column names of the table specified.

#### connection.columnsWithTypes(id: int, conName: string, tableName: string): Map<string, string>[]
- **Parameters**:
    + `id` : The ID of the session.
    + `conName` : The name of the connection.
    + `tableName` : The name of the table that this operation will use.
- **Return value**: An array of maps, where keys are column names and values are the corresponding type.
- **Functionality**: Returns an array of maps of the columns and their types of the specified table.

#### connection.availableOptions(id: int, conName: string): string[] 
- **Parameters**:
    + `id` : The ID of the session.
    + `conName` : The name of the connection.
- **Return value**: An array of `string` values.
- **Functionality**: Returns the available options of the currently used database.
#### connection.triggers(id: int, conName: string): string[]
- **Parameters**:
    + `id` : The ID of the session.
    + `conName` : The name of the connection.
- **Return value**: An array of `string` values.
- **Functionality**: Returns the names of the triggers if any.
#### connection.fk(id: int, conName: string, tableName: string):  Map<string, string>[]
- **Parameters**:
    + `id` : The ID of the session.
    + `conName` : The name of the connection.
    + `tableName` : The name of the table the operation will use.
- **Return value**: an array of maps, where keys are the foreign key column name and the value is the table the FK refers to.
- **Functionality**: Returns the foreign key columns and the tables they refer to. 
#### connection.remove(id: int, conName: string) 
- **Parameters**:
    + `id` : The ID of the session.
    + `conName` : The name of the connection.
- **Return value**: None.
- **Functionality**: Closes then removes the connection from the session. 

---

### Logging functions
Logging-related functions are listed here.\
***Note**: calling any of these functions before `request` results in returning an error.*
#### log.enable()
- **Parameters**: None.
- **Return value**: None.
- **Functionality**: Enables logging

#### log.disable()
- **Parameters**: None.
- **Return value**: None.
- **Functionality**: Disables logging

#### log.device.filename(path: string)
- **Parameters**: 
    + `path` : The file path that the logs will be written into.
- **Return value**: None.
- **Functionality**: You set the file that will receive log messages using this function.

#### log.device.socket(ip: string, port: int)
- **Parameters**:
    + `ip` : The IP address of the machine you want to send the logs to.
    + `port` : The port that the machine will be listening on.
- **Return value**: None.
- **Functionality**: In case you don't want to keep the logs in the databond server, you can change the destination to another server using this funciton.

#### log.info(msg: string)
- **Parameters**:
    + `msg` : The log message.
- **Return value**: None.
- **Functionality**: Logs a message of info type.

#### log.warn(msg: string)
- **Parameters**:
    + `msg` : The log message
- **Return value**: None
- **Functionality**: Logs a message of warn type

#### log.error(msg: string)
- **Parameters**:
    + `msg` : The log message
- **Return value**: None
- **Functionality**: Logs a message of error type

#### log.lastError(): string
- **Parameters**: None.
- **Return value**: A `string` value.
- **Functionality**:  returns the last error occurred in the logging.
